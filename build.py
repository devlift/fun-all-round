import os
import shutil
import codecs

print("Build Started.\n")
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

if os.path.exists("dist"):
    print("Clearing folder...")
    shutil.rmtree('dist/')


print("Downloading Packages...")
os.system('npm i')

print("Building English...")
os.system('ng build --prod --output-path=dist/en --baseHref /en/')

print("Building French...")
os.system('ng build --prod --configuration=fr --output-path=dist/fr --baseHref /fr/')

print("Copying assets...")
shutil.copyfile("index.html", "dist/index.html")
shutil.copytree("dist/en/assets/", "dist/assets/")

print("Fixing html...")
path = "dist/en/index.html"
en = codecs.open(path, 'r', 'utf8')
en = en.read()
en = en.replace('type="module"', 'type="text/javascript"')
en = en.replace('nomodule', 'nomodule type="text/javascript"')
with open(path, 'w') as filetowrite:
    filetowrite.write(en)
path = "dist/fr/index.html"

fr = codecs.open("dist/fr/index.html", 'r', 'utf8')
fr = fr.read()
fr = fr.replace('type="module"', 'type="text/javascript"')
fr = fr.replace('nomodule', 'nomodule type="text/javascript"')
with open(path, 'w') as filetowrite:
    filetowrite.write(fr)

print("\nBuild Finished.")

# 1. npm run build:en & npm ruin build:fr
# 2. move index.html file to root dist
# 3. copy assets folder from en to root
# 4. in each index.html the javascript needs typing added (type="text/javascript")
# 5. profit