import * as functions from 'firebase-functions';
import {
  getTime,
  getDate,
  getTotalPrice,
  getPhoneNumber } from '../../helpers/receiptHelper';
import { firestore } from "firebase-admin";
import { ReceiptData } from "../../models/receipt";
import * as crypto from 'crypto';
import { UserReceiptData } from "../../models/userReceiptData";
// import { UserData } from "../../models/userData";

const vision = require("@google-cloud/vision");
const visionClient = new vision.ImageAnnotatorClient();
const bucketName = functions.config().bucket.name;
const db: firestore.Firestore = firestore();

export const onStorageFinalize = functions.storage
  .bucket(bucketName)
  .object()
  .onFinalize(async (object: any) => {

    const filePath = object.name as string;

    const nameSplit = filePath.split("/");
    const id = nameSplit[1];

    const imageUri = `gs://${bucketName}/${filePath}`;

    const results = await visionClient.logoDetection(imageUri);

    const userRef = db.collection("users").doc(object.metadata.uid);
    const userReceiptRef = db.collection("users").doc(object.metadata.uid).collection("receipts").doc(id);

    // const doc = await userRef.get();
    // const user = doc.data() as UserData;

    // Text Reading
    const textResults = await visionClient.documentTextDetection(imageUri);
    const fullText = textResults[0].textAnnotations[0];
    const text = fullText ? fullText.description : null;

    // rejection
    if (textResults[0].textAnnotations.length === 0) {
      userReceiptRef.set({status: 'Rejected', reason: 'Unable to analyze image text'}, {merge: true}).catch(err => {
        console.log(err);
      });

      return;
    }

    // Detected logo is stored in retailer var
    const logos = results[0].logoAnnotations.map(
      (obj: { description: any }) => obj.description
    );
    let retailer = logos[0];

    if (retailer === undefined) {
      retailer = text[1]; //should be the first word at the top of the receipt
    }

    // might need to remove if we don't have any prod codes to scan for
    //  const prodCount = scanForProducts(text[0]);
    const receipt: ReceiptData = {
      user: userRef,
      uid: object.metadata.uid,
      purchaseDate: getDate(text[0]),
      purchaseTime: getTime(text[0]),
      total: getTotalPrice(text),
      retailerName: retailer,
      phoneNumber: getPhoneNumber(text[0]),
      userReceipt: userReceiptRef,
    };

    console.log(receipt);


    const hashString = receipt.purchaseDate + receipt.purchaseTime + receipt.retailerName + receipt.total.toString();
    const receiptHash = crypto.createHash('md5').update(hashString).digest("hex");

    const hashColRef = db.collection('receipt-hashes').doc(receiptHash);

    hashColRef.get()
      .then((docSnapshot) => {
        if (docSnapshot.exists) {
          console.log('receipt hash exists');

          userReceiptRef.set({status: 'Rejected', reason: 'Duplicate Upload'}, {merge: true}).catch(err => {
            console.log(err);
          });

          return;
        } else {

          hashColRef.set({}).catch((error) => {
            console.log(error);
          });

          const cde = receipt.userReceipt?.id;
          console.log(cde);

          const purgRef = db.collection('receipt-purgatory');
          receipt.status = 'pending';
          receipt.reason = '';
          receipt.count = 0;
          purgRef.add(receipt).then((receiptDocRef) => {
            userReceiptRef.get().then((snap) => {
              const rec = snap.data() as UserReceiptData;
              console.log(rec);
              if(rec) {
                receiptDocRef.set({
                  imageUrl: rec.url
                }, {merge: true}).catch(err => {
                  console.log(err);
                });
              }
            }).catch(err => {
              console.log(err);
            });
          }).catch(err => {
            console.log(err);
          });
        }
      }).catch((error) => {
        console.log(error);
      });

  });
