import * as functions from "firebase-functions";
import { UserData } from "../../models/userData";
import { firestore } from "firebase-admin";
// import { EmailTemplateType } from "../../models/emailTemplateType";
import * as dateformat from "dateformat";
const db: firestore.Firestore = firestore();

export const onUserUpdate = functions.firestore
        .document("/users/{id}")
        .onUpdate(
          async (
             change: functions.Change<functions.firestore.DocumentSnapshot>,
             context: functions.EventContext
           ) => {
            const prevUser = change.before.data() as UserData
            const user = change.after.data() as UserData;
            if (user.count) {
              if (user.count > 0) {
                const entrantsUserRef = db.collection('entrants').doc(change.after.id);
                const usersRef = db.collection("users").doc(change.after.id);
                // const entrantSnap = await entrantsUserRef.get();

                const userSnap = await usersRef.get();
                const userData = userSnap.data();
                if (userData) {
                  const timestamp = dateformat(Date.now(), "yyyy-mm-dd");
                  console.log(timestamp);
                  entrantsUserRef.set({userData, date: timestamp}, {merge: true}).catch(err => {
                    console.log(err);
                  });
                }

                let cnt = 0;
                console.log(cnt);
                if (prevUser.count) {
                  cnt = user.count - prevUser.count;
                } else {
                  cnt = user.count;
                }

              }
            }
           }
        );
