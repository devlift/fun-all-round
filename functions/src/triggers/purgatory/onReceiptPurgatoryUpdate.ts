import * as functions from "firebase-functions";
import { ReceiptData } from "../../models/receipt";
import { firestore } from "firebase-admin";
import * as crypto from 'crypto';
import { UserData } from "../../models/userData";

const db: firestore.Firestore = firestore();

export const onReceiptPurgatoryUpdate = functions.firestore
  .document("/receipt-purgatory/{id}")
  .onUpdate(
    async (
      change: functions.Change<functions.firestore.DocumentSnapshot>,
      context: functions.EventContext
    ) => {
      const receipt = change.after.data() as ReceiptData;
      const userRef = receipt.user;

      if (receipt.status === 'approved' || receipt.status === 'rejected') {

        let hashString = receipt.purchaseDate + receipt.purchaseTime + receipt.retailerName;
        const receiptTotal = receipt.total.toString(10);
        hashString += receiptTotal;
        const receiptHash = crypto.createHash('md5').update(hashString).digest("hex");

        const hashColRef = db.collection('receipt-hashes').doc(receiptHash);
        hashColRef.get()
          .then((docSnapshot) => {
            if (docSnapshot.exists) {
              return;
            } else {
              hashColRef.set({}).catch((error) => {
                console.log(error);
              });
            }
          }).catch(err => {
            console.log(err);
          });

        if (receipt.status === 'approved') {
          console.log(receipt);
          const receiptRef = db.collection('receipts');
          delete receipt.status;
          delete receipt.reason;

          receiptRef.add(receipt).then((receiptDocRef) => {

            if (receipt.userReceipt) {
              receipt.userReceipt.set({status: 'Accepted'}, {merge: true}).catch(err => {
                console.log(err);
              });
            }

            receipt.user.get().then((snap) => {
              const userData = snap.data() as UserData;
              if (userData) {
                let currCount = 0;
                if (userData.count) {
                  currCount = userData.count;
                }

                console.log('current product count: ' + currCount);
                if (receipt.count) {
                  userRef.set({
                    count: currCount + receipt.count
                  }, {merge: true}).catch(err => {
                    console.log(err);
                  });
                }

              }
            }).catch(err => {
              console.log(err);
            });

          }).catch(err => {
            console.error(err);
          });

          change.after.ref.delete().catch(err => {
            console.error(err);
          });
        }
        else if (receipt.status === 'rejected') {

          if (receipt.userReceipt && receipt.reason) {
            receipt.userReceipt.set({status: 'Rejected', reason: receipt.reason}, {merge: true}).catch(err =>
              {console.log(err);
            });
          }

          change.after.ref.delete().catch(err => {
            console.error(err);
          });

        }
      }

    }
  );
