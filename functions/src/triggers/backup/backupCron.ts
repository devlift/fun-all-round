import * as dateformat from "dateformat";
import { GoogleAuth } from "google-auth-library";

export const generateBackup = async () => {
  const auth = new GoogleAuth({
    scopes: [
      "https://www.googleapis.com/auth/datastore",
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  });
  const client = await auth.getClient();
  const timestamp = dateformat(Date.now(), "yyyy-mm-dd");
  const path = `${timestamp}`;
  const BUCKET_NAME = `lcl-devops-backup`;

  const projectId = await auth.getProjectId();
  const url = `https://firestore.googleapis.com/v1beta1/projects/${projectId}/databases/(default):exportDocuments`;
  const backup_route = `gs://${BUCKET_NAME}/${path}`;
  return client
    .request({
      url,
      method: "POST",
      data: {
        outputUriPrefix: backup_route
        // collectionsIds: [] // if you want to specify which collections to export, none means all
      }
    })
    .then(async res => {
      console.log(`Backup saved on folder on ${backup_route}`);
    })
    .catch(async e => {
      return Promise.reject({ message: e.message });
    });
};
