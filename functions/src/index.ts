import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
// import { generateBackup } from './triggers/backup/backupCron';

admin.initializeApp(functions.config().firebase)

export { onStorageFinalize } from './triggers/storage/onStorageFinalize'
export { onReceiptPurgatoryUpdate } from "./triggers/purgatory/onReceiptPurgatoryUpdate";
export { onUserUpdate } from "./triggers/user/onUserUpdate";

// cron job for db backups, daily
// export const automatedBackups = functions.pubsub
//   .schedule("0 0 * * *")
//   .onRun(generateBackup);
