const productCodes: string[] = [
  '058496444280',
  '058496448134',
  '058496448141',
  '058496448080',
  '058496453817',
  '058496448110'
];


// Various helper functions to check our receipt text with.

export function getPhoneNumber(text: any) {
  const phoneRegex = /(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}/;
  let found = "No Number Found";

  if (phoneRegex.test(text)) {
    found = text.match(phoneRegex);
    return found[0];
  }

  return found;
}

export function getTime(text: any) {
  const timeRegex = /(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]/;
  let found;

  if (timeRegex.test(text)) {
    found = text.match(timeRegex);
    return found[0];
  } else {
    found = "No visible time";
    return found;
  }
}

export function getTotalPrice(text: any) {
  const priceRegex = /\d{1,3}(?:[.,]\d{3})*(?:[.,]\d{2})/;

  let price = Number("0.00");

  for (let i = 1; i < text.length; i++) {
    if (text[i].match(priceRegex)) {
      if (text[i] > price) {
        price = Number(text[i]);
      }
    }
  }

  return price;
}

export function getDate(text: any) {
  const monthDayYearRegex = /((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))/;
  const dayMonthYearRegex = /([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}/;
  const yearMonthDayRegex = /([12]\d{3}\/(0[1-9]|1[0-2])\/(0[1-9]|[12]\d|3[01]))/;

  let found; // initializing variable

  if (dayMonthYearRegex.test(text)) {
    found = text.match(dayMonthYearRegex);
    return found[0];
  }
  //elseif matches yyyy/mm/dd || yyyy/dd/mm
  else if (yearMonthDayRegex.test(text)) {
    found = text.match(yearMonthDayRegex);
    return found[0];
  } else if (monthDayYearRegex.test(text)) {
    found = text.match(monthDayYearRegex);
    return found[0];
  } //no match
  else {
    found = "No Visible Date";
    return found;
  }
}

export function scanForProducts(text: string) {
  let counter = 0;
  console.log(text)
  productCodes.forEach(code => {
    if (text.includes(code)) {

      let prodCount = 0;
      const idx = text.indexOf(code);

      const prodCountStr = text.substring(idx-3, idx);
      console.log(prodCountStr);

      if(prodCountStr[0] === "(" && prodCountStr[2] === ")") {
        prodCount = parseInt(prodCountStr[1]);
        console.log(prodCountStr);
      } else {
        prodCount = 1;
      }


      counter += prodCount
    }
  });



  return counter;
}
