import { firestore } from "firebase-admin";

export interface ReceiptData {
  user: firestore.DocumentReference;
  uid: string;
  purchaseDate: string;
  purchaseTime: string;
  total: number;
  retailerName: string;
  phoneNumber: string;
  imageUrl?: string;
  status? : string;
  reason?: string;
  count?: number;
  userReceipt?: firestore.DocumentReference;
}
