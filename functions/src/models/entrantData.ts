import { firestore } from "firebase-admin";

export interface EntrantData {
  user: firestore.DocumentReference;
  entries: number;
  dateEntered: Date;
}
