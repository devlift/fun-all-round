import { firestore } from "firebase-admin";

export interface UserReceiptData {
  code: string;
  dateUploaded: Date;
  status: string;
  url: string;
  receipt?: firestore.DocumentReference;
  reason?: string;
}
