
export interface UserData {
  uid: string;
  firstName: string;
  lastName: string;
  email: string;
  province: string;
  postalCode: string;
  city: string;
  address: string;
  phone: string;
  age: number;
  recieveInfo: boolean;
  acceptRules: boolean;
  language: string;
  count?: number;
  dateRegistered: Date;
  noPurchaseZip?: string;
  recievedBGO?: boolean;
}
