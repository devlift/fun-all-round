
export interface SupportData {
  firstName: string;
  lastName: string;
  email: string;
  telephone: string;
  message: string;
  uid?: string;
  lang: string;
}
