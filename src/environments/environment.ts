// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  useProdURLS: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDXrWStpjKlbInbSHlX9VDRUtR5bE5NpSY',
    authDomain: 'mm-fun-all-around.firebaseapp.com',
    databaseURL: 'https://mm-fun-all-around.firebaseio.com',
    projectId: 'mm-fun-all-around',
    storageBucket: 'mm-fun-all-around.appspot.com',
    messagingSenderId: '348787270052',
    appId: '1:348787270052:web:5b5209fac9d30a5b7ca131',
    measurementId: 'G-H0N5N2VGG7'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
