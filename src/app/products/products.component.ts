import { Component, OnInit, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  isOpen = false;
  @Input() didProductsClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.didProductsClick.subscribe((data: boolean) => this.openProfile(data));
  }

  closeProfile() {
    this.isOpen = false;
  }

  openProfile(data: any) {
    this.isOpen = data;
  }

}
