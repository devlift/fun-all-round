import { Component, OnInit, OnDestroy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Toggle } from '../modal/modal.component';
import { FireStoreService } from 'src/services/firestore.service';
import { finalize } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AngularFireStorage } from 'angularfire2/storage';
import { UploadMetadata } from '@angular/fire/storage/interfaces';
import { BadToast, GoodToast, ToastList } from '../toasts/toasts.component';
import { GetLocale } from '../shared/get-locale';

@Component({
  selector: 'app-receipt-upload',
  templateUrl: './receipt-upload.component.html'
})

export class ReceiptUploadComponent implements OnInit, OnDestroy {

  lang: string;
  signedIn: boolean;

  private userSub: Subscription;
  private taskSub: Subscription;
  isUploading = false;
  selectedFiles: FileList;
  uploaded = false;
  public code: string;

  constructor(private afs: AngularFirestore, public fsService: FireStoreService, private storage: AngularFireStorage) {
    this.fsService.signedIn.subscribe(state => {
      this.signedIn = state;
    });
  }

  toggle(): void {
    Toggle('no-purchase');
  }

  guid() {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };

    // return id of format 'aaaaaaaa'
    return s4() + s4();
  }

  ngOnInit(): void {
    this.lang = GetLocale();
  }

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }

    if (this.taskSub) {
      this.taskSub.unsubscribe();
    }
  }

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.item(0)) {
      this.uploadpic();
    }
  }

  uploadpic() {
    this.fsService.log('upload_on_submit', 'on_submit');
    this.isUploading = true;
    const file = this.selectedFiles.item(0);
    this.code = this.guid();
    const metaData: UploadMetadata = {
      customMetadata: {
        uid: this.fsService.user.uid
      }
    };

    try {
      const uploadTask = this.storage.upload(
        `/${this.fsService.user.uid}/${this.code}`,
        file,
        metaData
      );

      const ref = this.storage.ref(`/${this.fsService.user.uid}/${this.code}`);

      this.taskSub = uploadTask
        .snapshotChanges()
        .pipe(
          finalize(() =>
            ref.getDownloadURL().subscribe(url => {
              this.fsService.setReceiptUploadedData(this.code, url);
              this.uploaded = true;
              // @ts-ignore
              window.addToast({
                id: this.code,
                ...GoodToast,
                // @ts-ignore
                msg: ToastList[this.lang].uploadS
              });
            })
          )
        )
        .subscribe();

      uploadTask.percentageChanges().subscribe(value => {
        if (value === 100) {
          this.isUploading = false;
        }
      });

    } catch (err) {
      this.isUploading = false;
      this.fsService.log('upload_failed', 'on_submit');
      // @ts-ignore
      window.addToast({
        id: this.code,
        ...BadToast,
        // @ts-ignore
        msg: ToastList[this.lang].uploadF
      });
    }

  }

}
