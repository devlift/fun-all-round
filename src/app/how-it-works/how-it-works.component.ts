import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FireStoreService } from 'src/services/firestore.service';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.scss']
})
export class HowItWorksComponent implements OnInit {

  @Output()
  uploadClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  signedIn: boolean;

  constructor(public fsService: FireStoreService) {
    this.fsService.signedIn.subscribe(state => {
      this.signedIn = state;
    });
  }

  ngOnInit(): void {

  }


  openProfile() {
    this.uploadClick.emit(true);
  }

}
