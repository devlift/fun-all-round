import { Component, OnInit, EventEmitter, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-ready-to-play',
  templateUrl: './ready-to-play.component.html',
  styleUrls: ['./ready-to-play.component.scss']
})
export class ReadyToPlayComponent implements OnInit, OnDestroy {

  lang: string;

  @Input() didChangeLang = new EventEmitter();

  constructor() {

  }

  ngOnInit() {
    this.didChangeLang.subscribe((data: string) => this.changeReadyLang(data));
  }

  changeReadyLang(data: string) {
    this.lang = data;
  }

  ngOnDestroy() {
    if (this.didChangeLang) {
      this.didChangeLang.unsubscribe();
    }
  }

}
