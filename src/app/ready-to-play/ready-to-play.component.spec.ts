import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadyToPlayComponent } from './ready-to-play.component';

describe('ReadyToPlayComponent', () => {
  let component: ReadyToPlayComponent;
  let fixture: ComponentFixture<ReadyToPlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadyToPlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadyToPlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
