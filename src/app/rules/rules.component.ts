import { Component, OnInit, Input, EventEmitter, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.scss']
})

export class RulesComponent implements OnInit, OnDestroy {

  lang: string;
  pdfUrl: string;
  @Input() didChangeLang = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.didChangeLang.subscribe((data: string) => this.changeRulesLang(data));
  }

  changeRulesLang(data: string) {
    this.lang = data;
    if (this.lang === 'fr') {
      this.pdfUrl = '/assets/longRules-French.pdf';
    } else {
      this.pdfUrl = '/assets/longRules-English.pdf';
    }
  }

  ngOnDestroy() {
    if (this.didChangeLang) {
      this.didChangeLang.unsubscribe();
    }
  }

}
