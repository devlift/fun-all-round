import { Component, OnInit, AfterViewInit, EventEmitter } from '@angular/core';
import { FireStoreService } from 'src/services/firestore.service';
import { Subject } from 'rxjs';

declare global {
  interface Document {
      documentMode?: any;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements AfterViewInit, OnInit {

  public signedIn: boolean;
  public lang: string;
  title = 'Fun All Around';

  didClick = new Subject();
  didProductsClick = new Subject();
  didChangeLang = new Subject();

  clickEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private fsService: FireStoreService) {

    this.fsService.signedIn.subscribe((status) => {
      this.signedIn = status;
    });

    const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
    if (isIE11) {
      alert('Legacy Browser IE is not supported, please navigate to this site using Edge, Google Chrome, or Firefox.');
    }

  }

  ngOnInit() {

  }

  getResolution(): object {
    const win = window;
    const doc = document;
    const docElem = doc.documentElement;
    const body = doc.getElementsByTagName('body')[0];
    const x = win.innerWidth || docElem.clientWidth || body.clientWidth;
    const y = win.innerHeight || docElem.clientHeight || body.clientHeight;
    return { x, y };
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    window.setUpAnimations = () => {

      const getResolution = () => {
        const win = window;
        const doc = document;
        const docElem = doc.documentElement;
        const body = doc.getElementsByTagName('body')[0];
        const x = win.innerWidth || docElem.clientWidth || body.clientWidth;
        const y = win.innerHeight || docElem.clientHeight || body.clientHeight;
        return { width: x, height: y };
      };

      const resolution = getResolution();

      if ('IntersectionObserver' in window && !(resolution.width <= 1400 && resolution.width >= 950)) {

        document.querySelectorAll('.parallax').forEach((elem) => observeParallax(elem, 3));
        document.addEventListener('scroll', () => parallaxElements.map((i) => parallax(i)));

        const config = {
          root: null,
          rootMargin: '0px',
          threshold: 0.5,
        };

        const observer = new IntersectionObserver(onChange, config);
        observer.observe(document.getElementById('page'));
        document.querySelectorAll('.fade-up').forEach((elem) => observer.observe(elem));

      } else {
        setTimeout(() => document.querySelectorAll('.fade-up').forEach((elem) =>
          elem.classList.add('animated')), 1000);
      }
    };

    const parallaxElements = [];

    const observeParallax = (element: Element, intensity: number) => {
      parallaxElements.push({element, intensity});
    };

    const onChange = (changes: any[], observer: { unobserve: (arg0: any) => void; }) => {
      changes.forEach((change: { intersectionRatio: number; target: any; }) => {
        if (change.intersectionRatio > 0) {
          fadeUp(change.target);
          observer.unobserve(change.target);
        }
      });
    };

    const parallax = async (item: { intensity: number; element: { style: { backgroundPositionY: string; }; }; }) => {
      const scrollTop = document.querySelectorAll('html')[0].scrollTop;
      const pos = scrollTop / item.intensity + 'px';
      item.element.style.backgroundPositionY = pos;
    };

    const fadeUp = (image: { classList: { add: (arg0: string) => any; }; }) => image.classList.add('animated');

    // @ts-ignore
    window.setUpAnimations();
  }

  openProductsEvent(event: any) {
    if (event) {
      this.didProductsClick.next(true);
    }
  }

  uploadClickEvent(event: any) {
    if (event) {
      this.emitClickEvent(event);
    }
  }

  emitClickEvent(event: any) {
    this.didClick.next(true);
  }

  changeLangEvent(event: any) {
    if (event) {
      this.emitChangeLangEvent(event);
    }
  }

  emitChangeLangEvent(event: any) {
    this.didChangeLang.next(event);
  }
}
