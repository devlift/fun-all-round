import { Component, OnInit } from '@angular/core';
import { Toggle } from '../modal.component';
import { GetLocale } from '../../shared/get-locale';
import { MustMatch } from '../../helpers/must-match.validator';
import { EMAIL_PATTERN, PHONE_PATTERN, POSTAL_PATTERN } from '../../shared/regex';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FireStoreService } from 'src/services/firestore.service';
import { ModalService } from 'src/services/modal.service';
import { BadToast, GoodToast, ToastList } from '../../toasts/toasts.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login-modal',
  templateUrl: './login-modal.component.html',
  styleUrls: ['./login-modal.component.scss']
})

export class LoginModalComponent implements OnInit {

  signup: boolean;
  lang: string;
  loginForm: FormGroup;
  signupForm: FormGroup;
  provinceSelect: HTMLSelectElement;
  isSubmittingForm: boolean;
  english: boolean;

  postalCodeMask = [/[A-Z]/i, /\d/, /[A-Z]/i, ' ', /\d/, /[A-Z]/i, /\d/];
  telephoneMask = [
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];

  guid() {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };

    // return id of format 'aaaaaaaa'
    return s4() + s4();
  }

  toggle(): void {
    Toggle();

  }

  hide(): void {
    Toggle('login');
  }

  OpenSignIn(): void {
    this.mdService.designateModal('login');
  }

  OpenSignUp() {
    this.mdService.designateModal('signup');

  }

  provinceChange(): void {
    if (!this.provinceSelect) {
      this.provinceSelect = document.getElementById('provinceSelectorLogin') as HTMLSelectElement;
    }
    if (this.provinceSelect && this.provinceSelect.value !== '' && this.provinceSelect.style.color !== '#000') {
      this.provinceSelect.style.color = '#000';
    }
  }

  constructor(private fb: FormBuilder,
              private fsService: FireStoreService,
              private translate: TranslateService,
              private mdService: ModalService) {

    this.english = true;

    this.mdService.loadModal.subscribe(modal => {
      if (modal === 'signup') {
        this.signup = true;
      } else if (modal === 'login') {
        this.signup = false;
      }
    });

    this.loginForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern(EMAIL_PATTERN)
      ]),
      password: this.fb.control('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });

    this.signupForm = this.fb.group({
      firstName: this.fb.control('', [Validators.required]),
      lastName: this.fb.control('', [Validators.required]),
      email: this.fb.control('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
      confirmEmail: this.fb.control('', [Validators.required]),
      postalCode: this.fb.control('', [Validators.required, Validators.pattern(POSTAL_PATTERN)]),
      province: this.fb.control('', [Validators.required]),
      city: this.fb.control('', [Validators.required]),
      address: this.fb.control('', [Validators.required]),
      telephone: this.fb.control('', [Validators.required, Validators.pattern(PHONE_PATTERN)]),
      age: this.fb.control('', [Validators.required]),
      password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: this.fb.control('', [Validators.required]),
      rules: this.fb.control(false, [Validators.pattern('true')])
    },
    {
      validators: [
        MustMatch('email', 'confirmEmail'),
        MustMatch('password', 'confirmPassword')
      ]
    });



  }

  ngOnInit(): void {
    this.lang = GetLocale();
    if (window.location.href.toLowerCase().includes('fr')) {
      this.lang = 'fr-CA';
      this.english = false;
    }


  }

  get lgF() {
    return this.loginForm.controls;
  }

  get suF() {
    return this.signupForm.controls;
  }

  async subForm() {
    if (this.signup) {
      this.isSubmittingForm = true;
      try {
        const registration = this.signupForm.value;
        const credential = await this.fsService.SignUp(
          registration.email,
          registration.password
        );
        const {
          confirmEmail,
          password,
          confirmPassword,
          ...user
        } = registration;
        await this.fsService.SetUserData(user, credential.user.uid);
        this.fsService.log('register_success', 'on_register');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...GoodToast,
          // @ts-ignore
          msg: ToastList[this.lang].registerS
        });
        this.signupForm.reset();

        this.hide();
      } catch (error) {
        this.isSubmittingForm = false;
        this.fsService.log('register_failed', 'on_register');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...BadToast,
          // @ts-ignore
          msg: ToastList[this.lang].registerF
        });
      }
    } else if (!this.signup) {
      this.isSubmittingForm = true;
      try {
        const user = this.loginForm.value;
        await this.fsService.SignIn(user.email, user.password);
        this.fsService.log('login_success', 'on_login');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...GoodToast,
          // @ts-ignore
          msg: ToastList[this.lang].loginS
        });
        this.loginForm.reset();

        this.hide();
      } catch (error) {
        this.fsService.log('login_failed', 'on_login');

        if (error.code === 'auth/wrong-password') {
          const id = this.guid();
          // @ts-ignore
          window.addToast({
            id,
            ...BadToast,
            // @ts-ignore
            msg: ToastList[this.lang].loginFP
          });
        } else if (error.code === 'auth/user-not-found') {
          const id = this.guid();
          // @ts-ignore
          window.addToast({
            id,
            ...BadToast,
            // @ts-ignore
            msg: ToastList[this.lang].loginFE
          });
        }

        this.isSubmittingForm = false;
      }
    }
  }
}
