import { Component, OnInit } from '@angular/core';
import {GetLocale} from '../../shared/get-locale';
import {Toggle} from '../modal.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-switch-language-modal',
  templateUrl: './switch-language-modal.component.html',
  styleUrls: ['./switch-language-modal.component.scss']
})
export class SwitchLanguageModalComponent implements OnInit {
  lang: string;
  toText: string;
  toUrl: string;
  message: string;

  toggle(): void {
    Toggle('locale');
  }

  switchLocale(): void {
    window.location.href = this.toUrl;
  }

  constructor() { }

  ngOnInit(): void {
    this.lang = GetLocale();
    if (window.location.href.includes('fr')) { this.lang = 'fr-CA'; }
    // tslint:disable-next-line: max-line-length
    this.message = this.lang === 'fr-CA' ? 'Il semble que la langue de votre navigateur ne corresponde pas à la page que vous avez chargée, souhaitez-vous changer de langue?' : 'It seems like your browser language doesn\'t match the page you loaded, would you like to switch languages?';


    if (this.lang === 'en-US') {
      this.toText = 'to FR';
      this.toUrl = environment.useProdURLS ? 'https://lheuredumatchsnickers.com/' : '/fr';
    } else {
      this.toText = 'to EN';
      this.toUrl = environment.useProdURLS ? 'https://snickersgametime.com/' : '/en';
    }
  }

}
