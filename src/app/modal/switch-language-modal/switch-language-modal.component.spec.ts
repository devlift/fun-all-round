import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchLanguageModalComponent } from './switch-language-modal.component';

describe('SwitchLanguageModalComponent', () => {
  let component: SwitchLanguageModalComponent;
  let fixture: ComponentFixture<SwitchLanguageModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchLanguageModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchLanguageModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
