import { Component, OnInit, Input } from "@angular/core";

const ToggleModal = (overlay, container) => {
  if (overlay.classList.contains("show")) {
    overlay.classList.remove("show");
    overlay.classList.add("transitioning");

    container.classList.remove("show");
    container.classList.add("transitioning");

    setTimeout(() => {
      overlay.classList.remove("transitioning");
      overlay.classList.add("hide");

      container.classList.remove("transitioning");
      container.classList.add("hide");

      //Gross
      let provinceSelect = document.getElementById(
        "provinceSelectorLogin"
      ) as HTMLSelectElement;
      if (provinceSelect && provinceSelect.value === "") provinceSelect.style.color = "#777";

      provinceSelect = document.getElementById(
        "provinceSelectorNPE"
      ) as HTMLSelectElement;
      if (provinceSelect && provinceSelect.value === "") provinceSelect.style.color = "#777";
      //Gross
    }, 620);
  } else {
    overlay.classList.remove("hide");
    overlay.classList.add("transitioning");

    container.classList.remove("hide");
    container.classList.add("transitioning");

    setTimeout(() => {
      overlay.classList.remove("transitioning");
      overlay.classList.add("show");

      container.classList.remove("transitioning");
      container.classList.add("show");

      //Gross
      let provinceSelect = document.getElementById(
        "provinceSelectorLogin"
      ) as HTMLSelectElement;
      if (provinceSelect && provinceSelect.value === "") provinceSelect.style.color = "#777";

      provinceSelect = document.getElementById(
        "provinceSelectorNPE"
      ) as HTMLSelectElement;
      if (provinceSelect && provinceSelect.value === "") provinceSelect.style.color = "#777";
      //Gross
    }, 100);
  }
};

export const Toggle = (id = undefined) => {
  const overlays = document.getElementsByClassName("modal-overlay");
  const containers = document.getElementsByClassName("modal-container");

  for (let i = 0; i < overlays.length; i++) {
    const overlay = overlays[i];
    const container = containers[i];

    if (!id || id === container.id) {
      ToggleModal(overlay, container);
    }
  }
};

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"]
})
export class ModalComponent implements OnInit {
  @Input() id: string;

  toggle(): void {
    Toggle(this.id);
  }

  constructor() {}

  ngOnInit(): void {}
}
