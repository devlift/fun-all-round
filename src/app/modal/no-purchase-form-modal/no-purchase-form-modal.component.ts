import { Component, OnInit } from '@angular/core';
import { Toggle } from '../modal.component';
import { GetLocale } from '../../shared/get-locale';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserData } from 'src/models/userData';
import { FireStoreService } from 'src/services/firestore.service';
import {
  EMAIL_PATTERN,
  PHONE_PATTERN,
  POSTAL_PATTERN
} from '../../shared/regex';
import { MustMatch } from '../../helpers/must-match.validator';
import { BadToast, GoodToast, ToastList } from '../../toasts/toasts.component';
@Component({
  selector: 'app-no-purchase-form-modal',
  templateUrl: './no-purchase-form-modal.component.html',
  styleUrls: ['./no-purchase-form-modal.component.scss']
})
export class NoPurchaseFormModalComponent implements OnInit {
  displayPin: boolean;
  signup: boolean;
  lang: string;
  isSubmittingForm: boolean;
  signupForm: FormGroup;
  loginForm: FormGroup;
  provinceSelect: HTMLSelectElement;
  npePin: string;
  english: boolean;

  constructor(
    private fb: FormBuilder,
    private db: AngularFirestore,
    private fsService: FireStoreService
  ) {
    this.english = true;
    this.signup = true;

    this.loginForm = this.fb.group({
      email: this.fb.control('', [
        Validators.required,
        Validators.pattern(EMAIL_PATTERN)
      ]),
      password: this.fb.control('', [
        Validators.required,
        Validators.minLength(6)
      ])
    });

    this.signupForm = this.fb.group({
      firstName: this.fb.control('', [Validators.required]),
      lastName: this.fb.control('', [Validators.required]),
      email: this.fb.control('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
      confirmEmail: this.fb.control('', [Validators.required]),
      postalCode: this.fb.control('', [Validators.required, Validators.pattern(POSTAL_PATTERN)]),
      province: this.fb.control('', [Validators.required]),
      city: this.fb.control('', [Validators.required]),
      address: this.fb.control('', [Validators.required]),
      telephone: this.fb.control('', [Validators.required, Validators.pattern(PHONE_PATTERN)]),
      age: this.fb.control('', [Validators.required]),
      password: this.fb.control('', [Validators.required, Validators.minLength(6)]),
      confirmPassword: this.fb.control('', [Validators.required]),
      rules: this.fb.control(false, [Validators.pattern('true')])
      // receiveInfo: this.fb.control(false)
    },
    {
      validators: [
        MustMatch('email', 'confirmEmail'),
        MustMatch('password', 'confirmPassword')
      ]
    });
  }

  provinceChange(): void {
    if (!this.provinceSelect) {
      this.provinceSelect = document.getElementById(
        'provinceSelectorLogin'
      ) as HTMLSelectElement;
    }
    if (
      this.provinceSelect &&
      this.provinceSelect.value !== '' &&
      this.provinceSelect.style.color !== '#000'
    ) {
      this.provinceSelect.style.color = '#000';
    }
  }

  ngOnInit(): void {
    this.lang = GetLocale();
    if (window.location.href.includes("fr")) {
      this.english = false;
      this.lang = "fr-CA";
    }
  }

  async subForm() {
    if (this.signup) {
      this.isSubmittingForm = true;
      try {
        const registration = this.signupForm.value;
        const credential = await this.fsService.SignUp(
          registration.email,
          registration.password
        );
        const {
          confirmEmail,
          password,
          confirmPassword,
          ...user
        } = registration;

        user.noPurchaseZip = this.guid();

        this.npePin = user.noPurchaseZip.toUpperCase();
        this.displayPin = true;

        await this.fsService.SetUserData(user, credential.user.uid);
        this.fsService.log('register_success', 'on_register');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...GoodToast,
          // @ts-ignore
          msg: ToastList[this.lang].registerS
        });
        this.signupForm.reset();

        setTimeout(() => { window.location.href = '/#profile'; window.location.reload(); }, 1500);
      } catch (error) {
        this.isSubmittingForm = false;
        this.fsService.log('register_failed', 'on_register');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...BadToast,
          // @ts-ignore
          msg: ToastList[this.lang].registerF
        });
      }
    } else if (!this.signup) {
      this.isSubmittingForm = true;
      try {
        const user = this.loginForm.value;
        await this.fsService.SignIn(user.email, user.password);
        this.fsService.log('login_success', 'on_login');
        const id = this.guid();
        // @ts-ignore
        window.addToast({
          id,
          ...GoodToast,
          // @ts-ignore
          msg: ToastList[this.lang].loginS
        });
        this.loginForm.reset();

        this.hide();
      } catch (error) {
        this.fsService.log('login_failed', 'on_login');
        if (error.code === 'auth/wrong-password') {
          const id = this.guid();
          // @ts-ignore
          window.addToast({
            id,
            ...BadToast,
            // @ts-ignore
            msg: ToastList[this.lang].loginFP
          });
        } else if (error.code === 'auth/user-not-found') {
          const id = this.guid();
          // @ts-ignore
          window.addToast({
            id,
            ...BadToast,
            // @ts-ignore
            msg: ToastList[this.lang].loginFE
          });
        }

        this.isSubmittingForm = false;
      }
    }
  }

  toggle(): void {
    this.npePin = '';
    this.displayPin = false;
    Toggle('no-purchase');
  }

  hide(): void {
    this.npePin = '';
    this.displayPin = false;
    Toggle('no-purchase');
  }

  toggleSignup(): void {
    this.displayPin = !this.displayPin;
  }
  toggleSignedin(): void {
    this.signup = !this.signup;
  }

  get lgF() {
    return this.loginForm.controls;
  }

  get suF() {
    return this.signupForm.controls;
  }

  guid() {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };

    // return id of format 'aaaaaaaa'
    return s4() + s4();
  }
}
