import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoPurchaseFormModalComponent } from './no-purchase-form-modal.component';

describe('NoPurchaseFormModalComponent', () => {
  let component: NoPurchaseFormModalComponent;
  let fixture: ComponentFixture<NoPurchaseFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoPurchaseFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPurchaseFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
