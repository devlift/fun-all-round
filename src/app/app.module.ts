import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CallToActionComponent } from './call-to-action/call-to-action.component';
import { ReadyToPlayComponent } from './ready-to-play/ready-to-play.component';
import { HowItWorksComponent } from './how-it-works/how-it-works.component';
import { ReceiptUploadComponent } from './receipt-upload/receipt-upload.component';
import { FaqComponent } from './faq/faq.component';
import { RulesComponent } from './rules/rules.component';
import { FooterComponent } from './footer/footer.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { ProfileComponent } from './profile/profile.component';
import { ModalComponent } from './modal/modal.component';
import { LoginModalComponent } from './modal/login-modal/login-modal.component';
import { NoPurchaseFormModalComponent } from './modal/no-purchase-form-modal/no-purchase-form-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FireStoreService } from 'src/services/firestore.service';
import { ModalService } from 'src/services/modal.service';
import { LoggedIn } from 'src/services/guards/logged-in.guard';
import { LoggedOut } from 'src/services/guards/logged-out.guard';
import { ToastsComponent } from './toasts/toasts.component';
import { SwitchLanguageModalComponent } from './modal/switch-language-modal/switch-language-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SwiperModule, SwiperConfigInterface, SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { FlexLayoutModule } from '@angular/flex-layout';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ProductsComponent } from './products/products.component';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'horizontal',
  threshold: 50,
  spaceBetween: 5,
  centeredSlides: true
};

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    CallToActionComponent,
    ReadyToPlayComponent,
    HowItWorksComponent,
    ReceiptUploadComponent,
    FaqComponent,
    RulesComponent,
    FooterComponent,
    ProfileComponent,
    ModalComponent,
    LoginModalComponent,
    NoPurchaseFormModalComponent,
    ToastsComponent,
    SwitchLanguageModalComponent,
    ProductsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    FormsModule, ReactiveFormsModule,
    BrowserAnimationsModule,
    SwiperModule,
    FlexLayoutModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [
    FireStoreService,
    LoggedIn,
    LoggedOut,
    ModalService, {
    provide: SWIPER_CONFIG,
    useValue: DEFAULT_SWIPER_CONFIG
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
