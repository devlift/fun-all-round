import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import { Toggle } from '../modal/modal.component';
import { FireStoreService } from 'src/services/firestore.service';
import { ModalService } from 'src/services/modal.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-call-to-action',
  templateUrl: './call-to-action.component.html',
  styleUrls: ['./call-to-action.component.scss']
})

export class CallToActionComponent implements OnInit, AfterViewInit {

  public signedIn: boolean;
  public lang: string;

  @ViewChild('english', { static: false } ) english: ElementRef;
  @ViewChild('french', { static: false } ) french: ElementRef;

  @Output()
  changeLang: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  uploadClick: EventEmitter<boolean> = new EventEmitter<boolean>();

  toggle(): void {
    Toggle('login');
  }

  purchaseToggle(): void {
    Toggle('no-purchase');
  }

  constructor(public fsService: FireStoreService,
              private renderer: Renderer2,
              private translate: TranslateService,
              private mdService: ModalService) {

    fsService.signedIn.subscribe(status => {
      this.signedIn = status;
    });

    translate.setDefaultLang('en');

  }

  ngAfterViewInit(): void {
    if (window.location.href.includes('duplaisiraprofusion')) {
      this.setLang('fr');
    } else {
      this.setLang('en');
    }
  }

  ngOnInit() {}

  setLang(lang: string) {
    this.translate.use(lang);
    this.lang = lang;
    if (lang === 'fr') {
      this.renderer.addClass(this.french.nativeElement, 'selected');
      this.renderer.removeClass(this.english.nativeElement, 'selected');
    } else {
      this.renderer.removeClass(this.french.nativeElement, 'selected');
      this.renderer.addClass(this.english.nativeElement, 'selected');
    }
    this.changeLang.emit(lang);

    if (this.signedIn) {
      this.fsService.updateUserLang(lang);
    }

  }

  async signOut() {
    await this.fsService.SignOut();
  }

  toggleLoginModal() {
    this.mdService.designateModal('login');
    this.toggle();
  }

  toggleSignupModal() {
    this.mdService.designateModal('signup');

    this.toggle();
  }

  openProfile() {
    this.uploadClick.emit(true);
  }

  toggleNoPurchase() {
    this.mdService.designateModal('no-purchase');
    this.purchaseToggle();
  }
}
