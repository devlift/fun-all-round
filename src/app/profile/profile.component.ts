import { Component, OnInit, AfterViewInit, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FireStoreService } from 'src/services/firestore.service';
import { UserProfileReceiptData } from 'src/models/userProfileReceiptData';
import { UserData } from 'functions/src/models/userData';
import { Subscription } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { Toggle } from '../modal/modal.component';
import { UploadMetadata } from '@angular/fire/storage/interfaces';
import { BadToast, GoodToast, ToastList } from '../toasts/toasts.component';
import { GetLocale } from '../shared/get-locale';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit, AfterViewInit, OnDestroy {

  colours: object;
  bgoAttempted: boolean;
  bgo: boolean;
  npePin: string;
  npePinGenerated: boolean;

  userData: UserData;
  userReceipts: UserProfileReceiptData[] = [];

  isOpen = false;
  @Input() public didClick = new EventEmitter();

  lang: string;
  signedIn: boolean;
  private userSub: Subscription;
  private taskSub: Subscription;
  isUploading = false;
  selectedFiles: FileList;
  uploaded = false;
  public code: string;

  constructor(private fsService: FireStoreService,
              private afs: AngularFirestore,
              private storage: AngularFireStorage) {

    this.fsService.signedIn.subscribe(state => {
      this.signedIn = state;
      if (this.signedIn) {
        this.initData();
      }
    });

  }

  ngOnInit() {
    this.lang = GetLocale();
    this.didClick.subscribe((data: any) => this.openProfile(data));
  }

  async initData(): Promise<void> {

    this.colours = {
      Accepted: '#8bc34a',
      Pending: '#fbae17',
      Rejected: '#ff5050',
    };

    this.bgo = false;
    this.bgoAttempted = false;
    this.npePinGenerated = false;

    this.userData = await this.fsService.returnUserData();

    // if (this.userData && this.userData.hasOwnProperty('recievedBGO')) {
    //   this.bgoAttempted = true;
    //   if (this.userData.recievedBGO) {
    //     this.bgo = true;
    //   }
    // }

    this.userReceipts = await this.fsService.getUserReceipts();
  }

  ngAfterViewInit(): void {
    // @ts-ignore
    // window.setUpAnimations();
    if (this.fsService.userData && this.fsService.userData.hasOwnProperty('noPurchaseZip')) {
      this.npePin = this.fsService.userData.noPurchaseZip.toUpperCase();
      this.npePinGenerated = true;
    }
  }

  closeProfile() {
    this.isOpen = false;
  }

  openProfile(data: any) {
    this.isOpen = data;
  }

  toggle(): void {
    Toggle('no-purchase');
  }

  guid() {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return s4() + s4();
  }

  chooseFiles(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.item(0)) {
      this.uploadpic();
    }
  }

  uploadpic() {
    this.fsService.log('upload_on_submit', 'on_submit');
    this.isUploading = true;
    const file = this.selectedFiles.item(0);
    this.code = this.guid();
    const metaData: UploadMetadata = {
      customMetadata: {
        uid: this.fsService.user.uid
      }
    };

    try {
      const uploadTask = this.storage.upload(
        `/${this.fsService.user.uid}/${this.code}`,
        file,
        metaData
      );

      const ref = this.storage.ref(`/${this.fsService.user.uid}/${this.code}`);

      this.taskSub = uploadTask
        .snapshotChanges()
        .pipe(
          finalize(() =>
            ref.getDownloadURL().subscribe(url => {
              this.fsService.setReceiptUploadedData(this.code, url);
              this.uploaded = true;
              // @ts-ignore
              window.addToast({
                id: this.code,
                ...GoodToast,
                // @ts-ignore
                msg: ToastList[this.lang].uploadS
              });
            })
          )
        )
        .subscribe();

      uploadTask.percentageChanges().subscribe(value => {
        if (value === 100) {
          this.isUploading = false;
        }
      });

    } catch (err) {
      this.isUploading = false;
      this.fsService.log('upload_failed', 'on_submit');
      // @ts-ignore
      window.addToast({
        id: this.code,
        ...BadToast,
        // @ts-ignore
        msg: ToastList[this.lang].uploadF
      });
    }

  }

  ngOnDestroy(): void {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }

    if (this.taskSub) {
      this.taskSub.unsubscribe();
    }
    if (this.didClick) {
      this.didClick.unsubscribe();
    }
  }

}
