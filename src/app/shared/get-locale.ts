export const GetLocale = () => {
  //@ts-ignore
  let lang = navigator.language || navigator.userLanguage;

  if (lang.includes("en")) lang = "en-US";
  if (lang.includes("fr")) lang = "fr-CA";

  if (lang !== "en-US" && lang !== "fr-CA") lang = "en-US";

  return lang;
};
