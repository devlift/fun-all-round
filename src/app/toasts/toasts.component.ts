import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Toast } from '../../models/toast';

export const BadToast = {
  icon: 'fas fa-times',
  color: '#F44336',
};

export const GoodToast = {
  icon: 'fas fa-check',
};

export const ToastList = {
  'en-US': {
    registerS: 'Registered successfully!',
    registerF: 'A problem occurred with your registration.',
    loginS: 'Logged in successfully!',
    loginFP: 'Failed to login, check your credentials.',
    loginFE: 'No account exists with that email.',
    uploadS: 'Sales receipt successfully uploaded!',
    // tslint:disable-next-line: max-line-length
    uploadF: 'A problem occured with your upload. Please check your connection and then try again. If the issue persists, you can contact Support to help you resolve the problem.'
  },
  'fr-CA': {
    registerS: 'Enregistré avec succès!',
    registerF: 'Un problème est survenu lors de votre inscription.',
    loginS: 'Connecté avec succès!',
    loginFP: 'Échec de la connexion, vérifiez vos informations d\'identification.',
    loginFE: 'Aucun compte n\'existe avec cet e-mail.',
    uploadS: 'Reçu de vente téléchargé avec succès!',
    // tslint:disable-next-line: max-line-length
    uploadF: 'Un problème est survenu pendant votre téléchargement Veuillez vérifier votre connexion, puis essayer de nouveau. Si le problème persiste, vous pouvez communiquer avec l\'équipe de soutien afin qu\'elle vous aide à résoudre le problème'
  }
};

@Component({
  selector: 'app-toasts',
  templateUrl: './toasts.component.html'
})
export class ToastsComponent implements OnInit {
  toasts: Array<Toast>;

  constructor(private ref: ChangeDetectorRef) {}

  addToast(toast: Toast) {
    // @ts-ignore
    if (!window.toastThis.toasts) { window.toastThis.toasts = []; }
    // @ts-ignore
    window.toastThis.toasts.push(toast);
    // @ts-ignore
    window.toastThis.ref.detectChanges();
    // @ts-ignore
    setTimeout(() => window.toastThis.removeToast(toast.id), 10000);
  }

  modArray(id: number) {
    this.toasts = this.toasts.filter(tst => tst.id !== id);
  }

  removeToast(id: number) {
    const toast = document.getElementById(`toast-${id}`);
    toast.classList.add('slide-out');
    setTimeout(() => {
      toast.remove();
      // @ts-ignore
      window.toastThis.toasts = window.toastThis.toasts.filter(
        tst => tst.id !== id
      );
      // @ts-ignore
      window.toastThis.ref.detectChanges();
    }, 600);
  }

  ngOnInit(): void {
    this.toasts = [];
    // @ts-ignore
    window.toastThis = this;
    // @ts-ignore
    window.addToast = this.addToast;
    // @ts-ignore
    window.removeToast = this.removeToast;
  }
}
