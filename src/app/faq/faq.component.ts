import { Component, OnInit, HostListener, Input, EventEmitter, OnDestroy, Output } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
})

export class FaqComponent implements OnInit, OnDestroy {

  lang: string;
  showMobile: boolean;
  pdfUrl: string;
  slides: Array<any>;
  frenchSlides: Array<any>;

  @Input() didChangeLang = new EventEmitter();
  @Output() openProducts: EventEmitter<boolean> = new EventEmitter<boolean>();

  config: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 2,
    keyboard: true,
    mousewheel: false,
    navigation: true,
    loop: true
  };

  mobileConfig: SwiperConfigInterface = {
    a11y: true,
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    mousewheel: false,
    navigation: true,
    loop: true
  };

  @HostListener('window:resize')
  onResize() {
    if (window.innerWidth > 767) {
      this.showMobile = false;
    } else {
      this.showMobile = true;
    }
  }

  constructor() {
    this.slides = [{
        title: 'Q: How do I enter?',
        textPre: "A: You can purchase one (1) qualifying M&M’S",
        copywrite: "®",
        textPost: "Chocolate Bar (any size, any variety) between August 10, 2020 and December 31, 2020. Keep your receipt, take a photo and upload it! Receipt must clearly show the qualifying product purchased, retailer name, date and transaction total amount."
      },
      {
        title: 'Q: Which products are participating in this Promotion?',
        text: "A: Participating products included in this promotion can be found ",
        productLink: 'here.'
      },
      {
        title: 'Q: Does my sweepstakes entry qualify me for both the Grand Prize and weekly draws?',
        text: "A: Yes, each qualifying purchase grants entry for the Grand Prize draw and weekly secondary prize draw."
      },
      {
        title: 'Q: When does the Grand Prize draw take place?',
        text: "A: The Grand Prize draw will be conducted on or around January 8, 2021 at approximately 02:00:00PM ET."
      },
      {
        title: 'Q: When do the weekly prize draws take place?',
        text: "A: Secondary Prizes will be drawn every Monday at approximately 02:00:00 PM ET, beginning August 17, 2020."
      },
      {
        title: 'Q: How many times can I submit the same valid receipt?',
        text: "A: You can submit a valid receipt only once."
      },
      {
        title: 'Q: How many times can I enter?',
        text: "A: There is no limit on entries!"
      },
      {
        title: 'Q: When does the promotion end?',
        text: "A: The promotion ends December 31, 2020 at 11:59:59 PM ET."
      },
      {
        title: 'Q: When is the last day to submit my receipt?',
        text: "A: The last day to submit your receipt is December 31, 2020 at 11:59:59 PM ET."
      },
      {
        title: 'Q: Can I enter if I did not purchase a participating product?',
        text: "A: Yes! Please see ",
        text2: " for details on No Purchase Entries.",
        entriesLink: 'Official Rules'
      },
    ];
    this.frenchSlides = [{
        title: 'Q. Comment puis-je participer?',
        textPre: "R. Vous devez acheter une (1) barre de chocolat M&M’S",
        copywrite: "MD",
        textPost: "admissible (format et variété au choix) entre le 10 août 2020 et le 31 décembre 2020. Conservez votre reçu, prenez-le en photo et téléchargez-le! Le reçu doit indiquer clairement le produit admissible acheté, le nom du détaillant, la date et le montant total de la transaction."
      },
      {
        title: 'Q. Quels sont les produits participants dans le cadre de cette promotion?',
        text: "R. Vous trouverez la liste des produits participants dans le cadre de cette promotion ",
        productLink: 'ici.'
      },
      {
        title: 'Q. Mon inscription au concours me rend-elle admissible au tirage au sort des grands prix et aux tirages au sort hebdomadaires?',
        text: "R. Oui, chaque achat admissible vous donne droit à une inscription au tirage au sort des grands prix et aux tirages au sort des deuxièmes prix hebdomadaires."
      },
      {
        title: 'Q. Quand le tirage au sort des grands prix aura-t-il lieu?',
        text: "R. Le tirage au sort des grands prix aura lieu le ou vers le 8 janvier 2021 aux environs de 14 h (HE)."
      },
      {
        title: 'Q. Quand les tirages au sort des prix hebdomadaires auront-ils lieu?',
        text: "R. Les tirages au sort des deuxièmes prix auront lieu chaque lundi, vers 14 h (HE), à compter du 17 août 2020."
      },
      {
        title: 'Q. Combien de fois puis-je soumettre le même reçu valide?',
        text: "R. Vous pouvez soumettre un reçu valide une seule fois."
      },
      {
        title: 'Q. Combien de fois puis-je participer?',
        text: "R. Il n’y a pas de limite quant au nombre d’inscriptions!"
      },
      {
        title: 'Q. Quand la promotion prend-elle fin?',
        text: "R. La promotion se termine le 31 décembre 2020 à 23 h 59 min 59 s (HE)."
      },
      {
        title: 'Q. Quelle est la dernière journée pour soumettre un reçu?',
        text: "R. La dernière journée pour soumettre votre reçu est le 31 décembre 2020 à 23 h 59 min 59 s (HE)."
      },
      {
        title: 'Q. Puis-je participer si je n’ai pas acheté un produit participant?',
        text: "R. Oui! Veuillez consulter le ",
        text2: " pour connaître la méthode de participation sans achat.",
        entriesLink: 'règlement officiel',
      },
    ];
  }

  ngOnInit(): void {
    if (window.innerWidth > 767) {
      this.showMobile = false;
    } else {
      this.showMobile = true;
    }
    this.didChangeLang.subscribe((data: string) => this.changeFaqLang(data));
  }

  changeFaqLang(data: string) {
    this.lang = data;
    if (this.lang === 'fr') {
      this.pdfUrl = '/assets/longRules-French.pdf';
    } else {
      this.pdfUrl = '/assets/longRules-English.pdf';
    }
  }

  openNoPurchase() {
    this.openProducts.emit(true);
  }

  ngOnDestroy() {
    if (this.didChangeLang) {
      this.didChangeLang.unsubscribe();
    }
  }

}
