import { Component, OnInit, OnDestroy, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit, OnDestroy {

  lang: string;
  privacyUrl: string;
  @Input() didChangeLang = new EventEmitter();

  constructor() {

  }

  ngOnInit() {
    this.didChangeLang.subscribe((data: string) => this.changeRulesLang(data));
  }

  changeRulesLang(data: string) {
    this.lang = data;
    if (this.lang === 'fr') {
      this.privacyUrl = 'https://www.mars.com/privacy-policy-france';
    } else {
      this.privacyUrl = 'https://www.mars.com/privacy';
    }
  }

  ngOnDestroy() {
    if (this.didChangeLang) {
      this.didChangeLang.unsubscribe();
    }
  }

}
