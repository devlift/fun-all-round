
export interface UserProfileReceiptData {
  code: string;
  dateUploaded: firebase.firestore.Timestamp;
  status: string;
  url: string;
  receipt?: any;
  reason?: string;
}
