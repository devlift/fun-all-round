
export interface UserReceiptData {
  code: string;
  dateUploaded: Date;
  status: string;
  url: string;
  receipt?: any;
  reason?: string;
}
