export interface Toast {
  icon: string;
  id: number;
  msg: string;
  color: string;
}
