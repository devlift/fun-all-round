import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';


@Injectable()
export class ModalService {

  private loadModalSource = new BehaviorSubject<string>('login');
  public loadModal = this.loadModalSource.asObservable();

  constructor() {}

  designateModal(modal: string) {
    this.loadModalSource.next(modal);
  }
}
