import { Injectable, OnDestroy } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Subscription, BehaviorSubject } from 'rxjs';
import { UserData } from 'src/models/userData';
import { UserReceiptData } from 'src/models/userReceiptData';
import { UserProfileReceiptData } from 'src/models/userProfileReceiptData';
import { take } from 'rxjs/operators';
import 'firebase/analytics';

@Injectable()
export class FireStoreService implements OnDestroy {

  private analytics: any;
  private userSub: Subscription;
  public user: firebase.User;

  private langSource = new BehaviorSubject<string>('en');
  private signedInSource = new BehaviorSubject<boolean>(false);

  public currLang = this.langSource.asObservable();
  public signedIn = this.signedInSource.asObservable();
  public userData: UserData;

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth) {

    this.analytics = firebase.analytics();

    this.userSub = this.afAuth.user.subscribe(user => {
      if (user) {
        this.user = user;
        this.signedInSource.next(true);
        this.getUserData();
      } else {
        this.user = null;
        this.userData = null;
        this.signedInSource.next(false);
      }
    });
  }

  ngOnDestroy() {
    if (this.userSub) {
      this.userSub.unsubscribe();
    }
  }

  // Sign up with email/password
  SignUp(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  SetUserData(user: UserData, userId) {
    const userRef: AngularFirestoreDocument<UserData> = this.afs.doc(
      `users/${userId}`
    );
    user.language = this.langSource.value;
    user.dateRegistered = new Date();
    return userRef.set(user, { merge: true });
  }

  SignIn(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  async SignOut() {
    await this.afAuth.auth.signOut();
  }

  log(event: string, eventName) {
    this.analytics.logEvent(event, { name: eventName });
  }

  updateLang(lg: string) {
    this.langSource.next(lg);
    if (this.signedIn) {
      this.updateUserLang(lg);
    }
  }

  async updateUserLang(lg: string) {
    await this.afs.doc(`users/${this.user.uid}`).update({language: lg});
    return this.getUserData();
  }

  async getUserData() {
    const userRef: AngularFirestoreDocument<UserData> = this.afs.doc(
      `users/${this.user.uid}`
    );
    userRef.get().toPromise().then(doc => {
      if (doc.exists) {
        const data = doc.data();
        this.userData = data as UserData;
      }
    });
  }

  async returnUserData(): Promise<UserData> {
    const userRef: AngularFirestoreDocument<UserData> = this.afs.doc(
      `users/${this.user.uid}`
    );

    const res = await userRef.get();

    // tslint:disable-next-line: no-shadowed-variable
    return new Promise(resolve => {
      res.pipe(
        take(1)
      ).subscribe(doc => {
        const uData = doc.data() as UserData;
        resolve(uData);
      });
    });
  }

  async setReceiptUploadedData(code: string, url: string) {
    const userReceiptsRef: AngularFirestoreDocument<UserReceiptData> = this.afs.doc(`users/${this.user.uid}/receipts/${code}`);
    const receiptData: UserReceiptData = {
      code,
      dateUploaded: new Date(),
      status: 'Pending',
      url
    };
    return userReceiptsRef.set(receiptData);
  }

  async getUserReceipts(): Promise<UserProfileReceiptData[]> {
    const userReceiptsRef = this.afs.collection(`users/${this.user.uid}/receipts`);

    const receipts: UserProfileReceiptData[] = [];

    const results = await userReceiptsRef.get();

    // tslint:disable-next-line: no-shadowed-variable
    return new Promise(resolve => {
      results.pipe(
        take(1)
      ).subscribe(snap => {
        snap.docs.forEach(doc => {
          receipts.push(doc.data() as UserProfileReceiptData);
        });
        resolve(receipts);
      });
    });

  }
}
