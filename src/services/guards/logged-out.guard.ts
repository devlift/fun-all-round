import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from '@angular/router';
import { Observable, pipe } from 'rxjs';
import { map, take, tap, first } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class LoggedOut implements CanActivate {
  constructor(private auth: AngularFireAuth, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {

    return this.auth.authState.pipe(
      map(authState => {
        if (authState !== null) {
          this.router.navigate(['home']);
        }
        return !authState;
      }),
      take(1)
    );
  }
}
